<?php

namespace DockerBundle\Services;

class ResponseParser
{
    /**
     * @param string $response
     *
     * @return array
     */
    public function parseProcessResponse($response)
    {
        $lines = explode("\n", $response);

        $content = [];
        foreach ($lines as $line) {
            if (empty($line) || strpos($line, 'REPOSITORY') !== false) {
                continue;
            }

            $matches = explode(Docker::DELIMITER, $line);

            if (empty($matches)) {
                continue;
            }

            $content[] = array_combine(Formatter::getProcessesFormat(), $matches);
        }

        return $content;
    }

    /**
     * @param string $response
     *
     * @return array
     */
    public function parseVolumesResponse($response)
    {
        $lines = explode("\n", $response);

        $content = [];
        foreach ($lines as $line) {
            if (empty($line) || strpos($line, 'REPOSITORY') !== false) {
                continue;
            }

            preg_match('/([a-zA-Z0-9\-\_]+)([\s]{2,})([a-zA-Z0-9\-\_]+)/', $line, $matches);

            if (empty($matches)) {
                continue;
            }

            $content[] = [
                $matches[1],
                $matches[3],
            ];
        }

        return $content;
    }

    /**
     * @param string $response
     *
     * @return array
     */
    public function parseImagesResponse($response)
    {
        $lines = explode("\n", $response);

        $content = [];
        foreach ($lines as $line) {
            if (empty($line) || strpos($line, 'REPOSITORY') !== false) {
                continue;
            }

            $matches = explode(Docker::DELIMITER, $line);

            if (empty($matches)) {
                continue;
            }

            $content[] = array_combine(Formatter::getImagesFormat(), $matches);
        }

        return $content;
    }

    /**
     * @param string $response
     *
     * @return array
     */
    public function parseStatsResponse($response)
    {
        $lines = explode("\n", $response);

        $content = [];
        foreach ($lines as $line) {
            if (empty($line) || strpos($line, 'REPOSITORY') !== false) {
                continue;
            }

            $matches = explode(Docker::DELIMITER, $line);

            if (empty($matches)) {
                continue;
            }

            $content[] = array_combine(Formatter::getStatsFormat(), $matches);
        }

        return $content;
    }

    /**
     * @param string $stringResponse
     *
     * @return array
     */
    public function parseEsClusterHealth($stringResponse)
    {
        return json_decode($stringResponse, true);
    }

    /**
     * @param string $stringResponse
     *
     * @return array
     */
    public function parseEsInfo($stringResponse)
    {
        return json_decode($stringResponse, true);
    }

    /**
     * @param string $response
     *
     * @return array
     */
    public function parseNetworksResponse($response)
    {
        $lines = explode("\n", $response);

        $content = [];
        foreach ($lines as $line) {
            if (empty($line) || strpos($line, 'REPOSITORY') !== false) {
                continue;
            }

            $matches = explode(Docker::DELIMITER, $line);

            if (empty($matches)) {
                continue;
            }

            $content[] = array_combine(Formatter::getNetworksFormat(), $matches);
        }

        return $content;
    }

    /**
     * @param string $stringResponse
     *
     * @return array
     */
    public function parseEsIndices($stringResponse)
    {
        $lines = explode("\n", $stringResponse);

        $rows = [];
        foreach ($lines as $line) {
            $rows[] = $this->reduceArrayElements(explode(' ', $line));
        }

        return $rows;
    }

    /**
     * @param array $array
     *
     * @return array
     */
    protected function reduceArrayElements($array)
    {
        $elements = [];
        foreach ($array as $item) {
            if ($item === '') {
                continue;
            }
            $elements[] = $item;
        }

        return $elements;
    }
}

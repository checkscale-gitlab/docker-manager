<?php

namespace DockerBundle\Services\Twig;

use DockerBundle\Services\ResponseTable;
use Kisphp\Twig\AbstractTwigFunction;
use Kisphp\Twig\IsSafeHtml;

class DrawVolumesFunction extends AbstractTwigFunction
{
    use IsSafeHtml;

    /**
     * @return string
     */
    protected function getExtensionName()
    {
        return 'drawVolumes';
    }

    /**
     * @return \Closure
     */
    protected function getExtensionCallback()
    {
        return function (array $tableData) {
            return $this->drawTable($tableData);
        };
    }

    /**
     * @param array $tableData
     *
     * @return string
     */
    protected function drawTable(array $tableData)
    {
        return ResponseTable::drawVolumes($tableData);
    }
}

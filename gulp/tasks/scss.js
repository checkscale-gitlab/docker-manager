'use strict';

let gulp = require('gulp');
let sass = require('gulp-sass');
let sourcemaps = require('gulp-sourcemaps');
let concat = require('gulp-concat');
let plumber = require('gulp-plumber');

let config = require('../config');

let css_config = {
  external: {
    sources: [
      'node_modules/bootstrap/dist/css/bootstrap.min.css',
      'node_modules/font-awesome/css/font-awesome.min.css'
    ],
    filename: 'external.css',
    target_directory: config.publicDir + 'css'
  },
  local: {
    sources: [
      config.sourceDir + 'scss/main.scss'
    ],
    filename: 'app.css',
    target_directory: config.publicDir + 'css'
  }
};

gulp.task('css:external', function(){
  return gulp.src(css_config.external.sources)
    .pipe(plumber())
    .pipe(concat(css_config.external.filename))
    .pipe(gulp.dest(css_config.external.target_directory))
  ;
});

gulp.task('css', function(){
  return gulp.src(css_config.local.sources)
    .pipe(plumber())
    .pipe(sass())
    .pipe(concat(css_config.local.filename))
    .pipe(sourcemaps.write('.'))
    .pipe(gulp.dest(css_config.local.target_directory))
  ;
});

gulp.task('watch:css', function(){
  gulp.watch([
    config.sourceDir + 'layout/css/*.*/*.css',
    config.sourceDir + 'scss/**/*.scss'
  ], ['css'])
});

gulp.task('css:copy:bootstrap', function(){
  gulp.src([
    'node_modules/bootstrap/dist/css/bootstrap.min.css.map'
  ], [{ base: './' }])
    .pipe(gulp.dest(config.publicDir + 'css/'))
  ;
});

let GR = require('kisphp-gulp-commander');

GR.addTask('css');
GR.addTask('css:external');
GR.addTask('css:copy:bootstrap');
GR.addWatch('watch:css');

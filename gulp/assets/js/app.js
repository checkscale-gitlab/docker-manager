$(document).ready(function() {
  require('./modules/tooltip').init();
  require('./modules/docker').init();
  require('./modules/upgrade').init();
});
